
var app = angular.module('ourwayApp', []);

app.controller('mainCtrl', function($scope, $http){
    
    $scope.headerText = 'Farsheed Ashouri';

});



app.controller('myinfoCtrl', function($scope, $http){
    
    $scope.githubLatestProject = null;


    $http.get('https://api.github.com/users/ourway/repos').success(function(resp){
        $scope.githubRepos = resp;
        if (resp){

            $scope.githubLatestProject = resp[0];

        }

    });

});
